﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomingRocket : MonoBehaviour {

    [HideInInspector] public float homingRange;
    [HideInInspector] public float turnSpeed;
    [HideInInspector] public float damage;
    [HideInInspector] public float moveSpeed;
    [HideInInspector] public float selfDestructTime;
    Transform tf;
    GameObject target;
    Vector3 vectorToTarget;

	// Use this for initialization
	void Start () {
        tf = transform;
        FindTarget();
        Invoke("SelfDestruct", selfDestructTime);
	}
	
	// Update is called once per frame
	void Update () {
        if (target != null)
        {
            vectorToTarget = target.transform.position - tf.position;   //find vector to target 
        }
        Move();
	}

    void FindTarget()
    {
        foreach (GameObject enemy in Singleton.gm.GetComponent<GameData>().enemies)
        {
            Vector3 vectorToEnemy = enemy.transform.position - tf.position; //get vector to current enemy
            RaycastHit hitData;
            if (Physics.Raycast(tf.position, vectorToEnemy, out hitData, homingRange))
            {
                if ((vectorToEnemy.magnitude < vectorToTarget.magnitude || target == null) && hitData.collider.gameObject.GetComponent<Identifier_Enemy>())
                {
                    target = enemy; //enemy becomes target of homing rocket 
                }
            }
        }
    }

    void Move()
    {
        tf.position += tf.forward * moveSpeed * Time.deltaTime;  //move forward
        if (target != null)
        {
            RotateToTarget();   //rotate toward the target 
            Debug.Log("Target");
        }
    }

    void RotateToTarget()
    {
        Quaternion goalRotation = Quaternion.LookRotation(vectorToTarget);  //set goal rotation
        tf.rotation = Quaternion.RotateTowards(tf.rotation, goalRotation, turnSpeed * Time.deltaTime);  //rotate towards target
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Health>())
        {
            other.gameObject.GetComponent<Health>().CurrentHealth -= damage;    //damage impacted object
        }
        Destroy(gameObject);    //destroy this homing rocket
    }

    void SelfDestruct()
    {
        Destroy(gameObject);    //destroy this homing rocket
    }
}