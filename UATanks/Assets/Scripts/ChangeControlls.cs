﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeControlls : MonoBehaviour {

    [SerializeField] List<string> controlNames; //name of controls
    [SerializeField] List<Text> showControls;   //show what controls are
    [SerializeField] bool p1Controls;

    int changeKey;          //control being changed
    bool changingControls;  //are controls being changed
    Controls controls;

    void Start()
    {
        if (p1Controls)
        {
            controls = Singleton.gm.GetComponent<GameData>().p1Controls.GetComponent<Controls>();
        }
        else
        {
            controls = Singleton.gm.GetComponent<GameData>().p2Controls.GetComponent<Controls>();
        }
        for (int i = 0; i < controlNames.Count; i++)
        {
            showControls[i].text = controlNames[i] + ": " + controls.controls[i].ToString();    //show initial controls
        }
    }

    private void OnGUI()
    {
        if (changingControls)
        {
            showControls[changeKey].text = controlNames[changeKey] + ": -";

            Event newKey = Event.current; //get curently pressed key
            if (newKey.isKey)
            {
                controls.controls[changeKey] = newKey.keyCode;                //change CHANGEKEY control to pressed key
                showControls[changeKey].text = controlNames[changeKey] + ": " + newKey.keyCode.ToString(); //change shown control key to new key
                changingControls = false;                                                                  //no longer changing controls
            }
            else if (newKey.isMouse)
            {
                if (newKey.button == 0)
                {
                    controls.controls[changeKey] = KeyCode.Mouse0;   //CHANGEKEY control set to left mouse
                    showControls[changeKey].text = controlNames[changeKey] + ": Left Mouse";
                }
                else if (newKey.button == 1)
                {
                    controls.controls[changeKey] = KeyCode.Mouse1;   //CHANGEKEY control set to right mouse
                    showControls[changeKey].text = controlNames[changeKey] + ": Right Mouse";
                }
                else if (newKey.button == 2)
                {
                    controls.controls[changeKey] = KeyCode.Mouse2;   //CHANGEKEY control set to middle mouse
                    showControls[changeKey].text = controlNames[changeKey] + ": Middle Mouse";
                }
                changingControls = false;                   //no longer changing controls
            }
        }
    }

    public void ChangeControl(int changedControl)
    {
        changingControls = true;    //now changing controls
        changeKey = changedControl; //set the control being changed
    }
}