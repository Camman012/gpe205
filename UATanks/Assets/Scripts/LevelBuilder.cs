﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LevelBuilder : MonoBehaviour {
    public enum LevelBuild { random, levelOfTheDay, seed };
    public LevelBuild buildMethod;
    [Header("Level Details")]
    public int seed;
	public int cols;
	public int rows;
	public Room[,] grid;
	public float tileWidth = 50;
	public float tileLength = 50;
	public List<GameObject> roomPrefabs;

    DateTime today;
	// Use this for initialization
	void Awake () {
        today = DateTime.Now.Date;
        buildMethod = (LevelBuild)Singleton.gm.GetComponent<GameData>().levelType;
        switch (buildMethod)
        {
            case LevelBuild.levelOfTheDay:
                UnityEngine.Random.InitState((int)today.Ticks);
                break;
            case LevelBuild.seed:
                UnityEngine.Random.InitState(seed);
                break;
            case LevelBuild.random:
                UnityEngine.Random.InitState((int)DateTime.Now.Ticks);
                break;
        }
		GenerateGrid ();
	}

	public void GenerateGrid () {
		grid = new Room[cols,rows]; // Setup our grid array
        // Once for each ROW
        for (int currentRow = 0; currentRow < rows; currentRow++) {
			// then... once for each column in that row
			for (int currentCol = 0; currentCol < cols; currentCol++) {
				Vector3 position = new Vector3 (currentCol * tileWidth, 0, currentRow * tileLength);        //set tile position
				GameObject prefabToSpawn = roomPrefabs [UnityEngine.Random.Range (0, roomPrefabs.Count)];               //get random room
				GameObject temp = Instantiate (prefabToSpawn, position, Quaternion.identity) as GameObject; //create selected room
				temp.name = "Tile "+currentCol+","+currentRow;  //set room name
				temp.transform.parent = transform.parent;       //set room parent

				// Open appropriate doors-
				Room tempRoom = temp.GetComponent<Room> ();
				if (currentCol != 0) {
					tempRoom.doorWest.SetActive (false);
				}
				if (currentCol != cols - 1) {
					tempRoom.doorEast.SetActive (false);
				}
				if (currentRow != 0) {
					tempRoom.doorSouth.SetActive (false);
				}
				if (currentRow != rows - 1) {
					tempRoom.doorNorth.SetActive (false);
				}
                //-
                
				grid[currentCol, currentRow] = tempRoom;    //store room into grid
			}	
		}
	}
}
