﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controls : MonoBehaviour {

    [HideInInspector] public int forward;   //representation of the forward key     (used for conveniency in other scripts)
    [HideInInspector] public int backward;  //representation of the back key        (used for conveniency in other scripts)
    [HideInInspector] public int right;     //representation of the right key       (used for conveniency in other scripts)
    [HideInInspector] public int left;      //represntation of the left key         (used for conveniency in other scripts)
    [HideInInspector] public int fireBullet;//representation of the fire key        (used for conveniency in other scripts)
    [HideInInspector] public int pause;     //representation of the pause key       (used for conveniency in other scripts)
    [HideInInspector] public int fireRocket;//representation of the fire rocket key (used for conveniency in other scripts)
    public List<KeyCode> controls;          //list of all controls

    void Awake()
    {
        forward = 0;
        left = 1;
        backward = 2;
        right = 3;
        fireBullet = 4;
        fireRocket = 5;
        pause = 6;
    }
}
