﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighScore : MonoBehaviour {

	// Use this for initialization
	void Start () {
        SetHighScore(PlayerPrefs.GetFloat("HighScore"));
	}

    public void SetHighScore(float score)
    {
        if (score > Singleton.gm.GetComponent<GameData>().highScore)
        {
            Singleton.gm.GetComponent<GameData>().highScore = score;
        }
    }

    public void SetPlayerScore(GameObject player)
    {
        if (player == Singleton.gm.GetComponent<GameData>().players[0])
        {
            Singleton.gm.GetComponent<GameData>().player1Score = player.GetComponent<TankData>().playerPoints;
        }
        else
        {
            Singleton.gm.GetComponent<GameData>().player2Score = player.GetComponent<TankData>().playerPoints;
        }
    }

    public void SaveHighScore()
    {
        PlayerPrefs.SetFloat("HighScore", Singleton.gm.GetComponent<GameData>().highScore);
    }
}