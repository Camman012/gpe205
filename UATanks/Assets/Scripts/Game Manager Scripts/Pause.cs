﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour {

    public GameObject pauseScreen;
    
    [HideInInspector] public bool isPaused = false;

    public void DoPause()
    {
            if (isPaused)
            {
                pauseScreen.SetActive(false);             //Unpause screen
                Cursor.visible = false;                   //remove cursur visibility
                Cursor.lockState = CursorLockMode.Locked; //relock cursur
                Time.timeScale = 1;                       //resume world
            }
            else
            {
                pauseScreen.SetActive(true);            //pause screen
                Cursor.visible = true;                  //allow cursur visibility
                Cursor.lockState = CursorLockMode.None; //allow cursur movement
                Time.timeScale = 0;                     //stop world
            }
            isPaused = !isPaused;
    }
}
