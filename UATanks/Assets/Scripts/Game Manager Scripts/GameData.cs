﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData : MonoBehaviour {

    public int numberOfEnemies;
    public bool twoPlayer;
    public GameObject p1Controls;
    public GameObject p2Controls;
    public float highScore;
    public float player1Score;
    public float player2Score;
    public GameObject musicContainer;
    public GameObject sfxContainer;
    public float masterVolume;
    public float sfxVolume;
    public float musicVolume;

    [HideInInspector] public List<GameObject> players;
    [HideInInspector] public List<GameObject> enemies;
    [HideInInspector] public List<GameObject> playerSpawnLocations;
    [HideInInspector] public List<GameObject> enemySpawnLocations;
    [HideInInspector] public int levelType;
    [HideInInspector] public int levelSeed;
}