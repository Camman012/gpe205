﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton : MonoBehaviour {

    public static Singleton gm;         //set singlton object

    private void Awake()
    {
        if (gm == null)
        {
            gm = this;                  //this object becomes the gm singleton
        }
        else
        {
            Destroy(gameObject);        //destroy this object
        }
    }
    private void Start()
    {
        DontDestroyOnLoad(gameObject);  //do not destroy on load
    }
}
