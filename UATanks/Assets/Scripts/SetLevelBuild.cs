﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetLevelBuild : MonoBehaviour {

    Dropdown options;
    [SerializeField] InputField seedEntry;

    private void Start()
    {
        options = gameObject.GetComponent<Dropdown>();
    }

    public void SelectOption()
    {
        Singleton.gm.GetComponent<GameData>().levelType = options.value;
        if (options.value == 2)
        {
            seedEntry.interactable = true;
        }
        else
        {
            seedEntry.interactable = false;
        }
    }

    public void OnSeedChange()
    {
        Singleton.gm.GetComponent<GameData>().levelSeed = int.Parse(seedEntry.text);
    }
}