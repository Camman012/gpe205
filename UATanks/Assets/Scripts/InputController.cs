﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour {

	TankData data;
    Controls controls;
    float bulletTimer;
    float RocketTimer;

    KeyCode forward;
    KeyCode left;
    KeyCode back;
    KeyCode right;
    KeyCode primary;
    KeyCode secondary;
    KeyCode pause;

    private void Awake()
    {
        Singleton.gm.GetComponent<GameData>().players.Add(gameObject);
    }

    // Use this for initialization
    void Start () {
		data = GetComponent<TankData> ();

        bulletTimer = data.timeBetweenShots;
        RocketTimer = data.timeBetweenHomingShots;

        if (gameObject == Singleton.gm.GetComponent<GameData>().players[0])
        {
            controls = Singleton.gm.GetComponent<GameData>().p1Controls.GetComponent<Controls>();
        }
        else if (gameObject == Singleton.gm.GetComponent<GameData>().players[1])
        {
            controls = Singleton.gm.GetComponent<GameData>().p2Controls.GetComponent<Controls>();
        }

        Cursor.visible = false;                   //remove cursur visibility
        Cursor.lockState = CursorLockMode.Locked; //lock cursur

        //get controls based on indicated controls object
        forward = controls.controls[controls.forward];
        left = controls.controls[controls.left];
        back = controls.controls[controls.backward];
        right = controls.controls[controls.right];
        primary = controls.controls[controls.fireBullet];
        secondary = controls.controls[controls.fireRocket];
        pause = controls.controls[controls.pause];
    }

    

    // Update is called once per frame
    void Update () {
        if (!Singleton.gm.GetComponent<Pause>().isPaused)
        {
            Vector3 moveVector = Vector3.zero;
            if (Input.GetKey(forward))
            {
                moveVector = data.mover.tf.forward * data.forwardMoveSpeed;  //move forward
            }
            if (Input.GetKey(back))
            {
                moveVector = -data.mover.tf.forward * data.backwardMoveSpeed;   //move backward
            }
            if (Input.GetKey(right))
            {
                data.mover.Rotate(1.0f);    //rotate right
            }
            if (Input.GetKey(left))
            {
                data.mover.Rotate(-1.0f);   //rotate left
            }
            if (Input.GetKey(primary))
            {
                if (bulletTimer >= data.timeBetweenShots)
                {
                    data.shooter.FireBullet();   //fire shell
                    bulletTimer = 0;         //reset fire delay timer
                }
            }
            if (Input.GetKeyDown(pause))
            {
                Singleton.gm.gameObject.GetComponent<Pause>().DoPause();    //pause game
            }
            if (Input.GetKeyDown(secondary))
            {
                data.shooter.FireHomingRocket();    //fire rocket
                RocketTimer = 0;                    //reset rocket fire delay timer
            }
            bulletTimer += Time.deltaTime;   //countdown for fire delay
            RocketTimer += Time.deltaTime;   //countdown for rocket fire delay
            data.mover.Move(moveVector);   //move in specified direction 
        }
	}

    private void OnDestroy()
    {
        Singleton.gm.GetComponent<HighScore>().SetPlayerScore(gameObject);
        Singleton.gm.GetComponent<GameData>().players.Remove(gameObject);
        foreach (GameObject player in Singleton.gm.GetComponent<GameData>().players)
        {
            if (player != gameObject)
            {
                player.GetComponent<CameraChange>().SetCameraRect();
            }
        }
        if (Singleton.gm.GetComponent<GameData>().players.Count <= 0)
        {
            Singleton.gm.GetComponent<HighScore>().SaveHighScore();
            Singleton.gm.GetComponent<SceneChanger>().LoadScene(3);
        }
    }
}
