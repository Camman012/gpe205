﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetVolume : MonoBehaviour {

    AudioSource volume;

    private void Start()
    {
        volume = gameObject.GetComponent<AudioSource>();
    }

    // Use this for initialization
    void Update () {
        volume.volume = Singleton.gm.GetComponent<GameData>().musicVolume * Singleton.gm.GetComponent<GameData>().masterVolume;
	}
}
