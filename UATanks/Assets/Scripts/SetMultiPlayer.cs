﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetMultiPlayer : MonoBehaviour {

    public Text text;

    public void OnChangeMode()
    {
        if (Singleton.gm.GetComponent<GameData>().twoPlayer)
        {
            text.text = "Single Player";
            Singleton.gm.GetComponent<GameData>().twoPlayer = false;
        }
        else
        {
            text.text = "Two Player";
            Singleton.gm.GetComponent<GameData>().twoPlayer = true;
        }
    }
}