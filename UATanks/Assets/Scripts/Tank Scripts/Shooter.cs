﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour {
    
    TankData data;
    public GameObject shellLocation;

    float noiseVolume;

	// Use this for initialization
	void Start () {
        data = GetComponent<TankData>();
        noiseVolume = Singleton.gm.GetComponent<GameData>().sfxVolume * Singleton.gm.GetComponent<GameData>().masterVolume;
	}
	
	public void FireBullet()
    {
        GameObject newShell;
		newShell = Instantiate(data.shellObject, shellLocation.transform.position, shellLocation.transform.rotation) as GameObject; //create shell object
        //set shell specs based on tank data
        newShell.GetComponent<Shell>().spawnLocation = shellLocation;
        newShell.GetComponent<Shell>().damage = data.shellDamage;
        newShell.GetComponent<Shell>().speed = data.shellSpeed;
        newShell.GetComponent<Shell>().selfDestructTime = data.selfDestructTime;
        newShell.GetComponent<Shell>().shooter = gameObject;
        Singleton.gm.GetComponent<GameData>().musicContainer.GetComponent<AudioSource>().PlayOneShot(data.shootSound, noiseVolume);
    }

    public void FireHomingRocket()
    {
        if (data.numberOfHomingBullets > 0)
        {
            GameObject newRocket;
            newRocket = Instantiate(data.homingBulletObject, shellLocation.transform.position - shellLocation.transform.up, data.mover.tf.rotation) as GameObject; //create homing bullet
            //set homing rocket specs based on tank data
            newRocket.GetComponent<HomingRocket>().damage = data.homingBulletDamage;
            newRocket.GetComponent<HomingRocket>().moveSpeed = data.homingBulletSpeed;
            newRocket.GetComponent<HomingRocket>().turnSpeed = data.homingBulletTurnSpeed;
            newRocket.GetComponent<HomingRocket>().homingRange = data.homingBulletRange;
            newRocket.GetComponent<HomingRocket>().selfDestructTime = data.HomingSelfDestructTime;

            data.numberOfHomingBullets--;
        }
    }
}