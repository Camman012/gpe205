﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

    [HideInInspector] public GameObject sendPointsTo;

    float noiseVolume;
    TankData data;
    float currentHealth;
    public float CurrentHealth
    {
        get { return currentHealth; }
        set
        {
            currentHealth = value;
            if (currentHealth <= 0)
            {
                Singleton.gm.GetComponent<GameData>().sfxContainer.GetComponent<AudioSource>().PlayOneShot(data.deathSound, noiseVolume);
                GivePoints();
            }
            if (currentHealth > data.maxHealth)
            {
                currentHealth = data.maxHealth;
            }
        }
    }

	// Use this for initialization
	void Start () {
        data = GetComponent<TankData>();
        CurrentHealth = data.maxHealth;
        noiseVolume = Singleton.gm.GetComponent<GameData>().sfxVolume * Singleton.gm.GetComponent<GameData>().masterVolume;
	}

    void GivePoints()
    {
        sendPointsTo.GetComponent<TankData>().playerPoints += data.enemyPoints;
        Destroy(gameObject);
    }
}