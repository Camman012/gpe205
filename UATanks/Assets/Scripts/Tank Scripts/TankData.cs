﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankData : MonoBehaviour {

	[HideInInspector] public Mover mover;
	[HideInInspector] public InputController controller;
    [HideInInspector] public Health health;
    [HideInInspector] public Shooter shooter;
    [HideInInspector] public AudioSource auSource;

	[Header("Tank Stats")]
    [Tooltip("In meters/second")]
	public float forwardMoveSpeed;
    [Tooltip("In meters/second")]
    public float backwardMoveSpeed;
    [Tooltip("In degrees/second")]
    public float turnSpeed;
    public float maxHealth;
    [Header("Normal Bullet Stats")]
    public GameObject shellObject;
    public float shellDamage;
    public float shellSpeed;
    public float timeBetweenShots;
    [Tooltip("Time in seconds until shell automatically destroys itslef.")]
    public float selfDestructTime;
    [Header("Homing Bullet Stats")]
    public GameObject homingBulletObject;
    public float homingBulletDamage;
    public float homingBulletSpeed;
    public float homingBulletTurnSpeed;
    public float homingBulletRange;
    public int numberOfHomingBullets;
    public float timeBetweenHomingShots;
    public float HomingSelfDestructTime;
    [Header("Sounds")]
    public AudioClip shootSound;
    public AudioClip deathSound;
    [Header("Other")]
    public float playerPoints;
    public float enemyPoints;
    public string player;

    private void Start()
    {
        mover = GetComponent<Mover>();
        controller = GetComponent<InputController>();
        health = GetComponent<Health>();
        shooter = GetComponent<Shooter>();
        auSource = GetComponent<AudioSource>();
    }
}