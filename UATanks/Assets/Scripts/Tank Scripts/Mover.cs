﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class Mover : MonoBehaviour {
    [HideInInspector] public TankData data;
	[HideInInspector] public Transform tf;
	[HideInInspector] public CharacterController cc;

	// Use this for initialization
	void Start () {
        data = gameObject.GetComponent<TankData>();
		tf = transform;
		cc = GetComponent<CharacterController> ();
	}

	public void Move(Vector3 speedAndDirection)
	{
		cc.SimpleMove(speedAndDirection);   //move in specified direction
	}
    public void Rotate(float direction)
    {
        tf.Rotate(new Vector3(0, (data.turnSpeed * direction)*Time.deltaTime, 0));  //rotate in specified direction
    }
	public void RotateTowards(Vector3 newDirection)
	{
		Quaternion goalRotation;
		goalRotation = Quaternion.LookRotation (newDirection);
		tf.rotation = Quaternion.RotateTowards (tf.rotation, goalRotation, data.turnSpeed * Time.deltaTime);
	}
}