﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Shell : MonoBehaviour {
    
    public AudioClip bulletImpact;
    [HideInInspector] public float damage;
    [HideInInspector] public float speed;
    [HideInInspector] public float selfDestructTime;
    [HideInInspector] public GameObject spawnLocation;
    [HideInInspector] public GameObject shooter;
    Transform tf;

	// Use this for initialization
	void Start () {
        tf = transform;
        tf.position += (new Vector3(0, spawnLocation.transform.position.y - tf.position.y));    //correct shell hight
        Invoke("SelfDestruct", selfDestructTime);   //destroy shell after x time
	}
	
	// Update is called once per frame
	void Update () {
        tf.position += -tf.up * (speed*Time.deltaTime);   //move bullet
	}
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.GetComponent<Health>())
        {
            collision.collider.gameObject.GetComponent<Health>().CurrentHealth -= damage;   //deal damage
            collision.collider.gameObject.GetComponent<Health>().sendPointsTo = shooter;    //set who points are sent to
        }
        AudioSource.PlayClipAtPoint(bulletImpact, tf.position); //play imppact sound
        Destroy(gameObject);    //destroy shell
    }

    void SelfDestruct()
    {
        Destroy(gameObject);    //destroy shell
    }
}
