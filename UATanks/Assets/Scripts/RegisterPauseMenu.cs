﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegisterPauseMenu : MonoBehaviour {

    [SerializeField] GameObject pauseScreen;

	// Use this for initialization
	void Start () {
        Singleton.gm.GetComponent<Pause>().pauseScreen = pauseScreen;
	}
}
