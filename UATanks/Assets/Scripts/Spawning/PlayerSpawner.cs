﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour {

    public GameObject player1;
    public GameObject player2;

    [HideInInspector] public List<GameObject> spawnLocations;
    GameObject spawn;

	// Use this for initialization
	void Start () {
        spawnLocations = Singleton.gm.GetComponent<GameData>().playerSpawnLocations;
        spawnPlayer(player1);
        if (Singleton.gm.GetComponent<GameData>().twoPlayer)
        {
            spawnPlayer(player2); 
        }
	}

    void spawnPlayer(GameObject player)
    {
        while (true)
        {
            spawn = spawnLocations[Random.Range(0, spawnLocations.Count)];
            if (!spawn.GetComponent<PlayerSpawnLocation>().hasEnemySpawned && !spawn.GetComponent<PlayerSpawnLocation>().hasPlayerSpawned)
            {
                Instantiate(player, spawn.transform.position, Quaternion.identity);
                spawn.GetComponent<PlayerSpawnLocation>().hasPlayerSpawned = true;
                break;
            } 
        }
    }
}