﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    public List<GameObject> enemyPrefabs;
    GameObject enemy;

    [HideInInspector] public List<GameObject> spawnLocations;
    GameObject spawn;

	// Use this for initialization
	void Start () {
        //Invoke("BeginSpawn", .5f);
        spawnLocations = Singleton.gm.GetComponent<GameData>().enemySpawnLocations;
        for (int i = 0; i < Singleton.gm.GetComponent<GameData>().numberOfEnemies; i++)
        {
            SpawnEnemy();
        }
    }

    void SpawnEnemy ()
    {
        while (true)
        {
            spawn = spawnLocations[Random.Range(0, spawnLocations.Count)];
            if (!spawn.GetComponent<EnemySpawnLocation>().hasEnemySpawned)
            {
                enemy = enemyPrefabs[Random.Range(0, enemyPrefabs.Count)];
                Instantiate(enemy, spawn.transform.position, Quaternion.identity);
                spawn.GetComponent<EnemySpawnLocation>().playerSpawnLocation.GetComponent<PlayerSpawnLocation>().hasEnemySpawned = true;
                break;
            } 
        }
    }
}
