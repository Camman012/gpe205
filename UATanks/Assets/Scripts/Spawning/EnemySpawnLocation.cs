﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnLocation : MonoBehaviour {

    [HideInInspector] public bool hasEnemySpawned = false;
    public GameObject playerSpawnLocation;

	// Use this for initialization
	void Awake () {
        Singleton.gm.GetComponent<GameData>().enemySpawnLocations.Add(gameObject);
	}
}
