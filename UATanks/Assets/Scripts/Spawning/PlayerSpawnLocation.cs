﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawnLocation : MonoBehaviour {

    [HideInInspector] public bool hasEnemySpawned = false;
    [HideInInspector] public bool hasPlayerSpawned = false;

	// Use this for initialization
	void Awake () {
        Singleton.gm.GetComponent<GameData>().playerSpawnLocations.Add(gameObject);
	}
}
