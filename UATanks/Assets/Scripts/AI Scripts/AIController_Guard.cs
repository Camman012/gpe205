﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController_Guard : AIController {

    public float seekDistance;

	// Use this for initialization
	void Start () {
        DoStart();  //preform base start function
	}
	
	// Update is called once per frame
	void Update () {
        if (!Singleton.gm.GetComponent<Pause>().isPaused)
        {
            DoUpdate(); //preform base update function

            switch (aiState)
            {
                case AIStates.idle:
                    DoIdle();
                    if (CanSeePlayer())
                    {
                        if (vectorToTarget.magnitude <= seekDistance)
                        {
                            ChangeAIState(AIStates.idle, AIStates.seekPlayer);
                        }
                        else
                        {
                            ChangeAIState(AIStates.idle, AIStates.shoot);
                        }
                    }
                    break;
                case AIStates.seekAndShoot:
                    if (CanMoveForward())
                    {
                        if (vectorToTarget.magnitude <= seekDistance)
                        {
                            DoSeekPlayer();
                            if (angleToTarget <= shootFOV)
                            {
                                DoShoot();
                            }
                            else
                            {
                                ChangeAIState(AIStates.seekAndShoot, AIStates.seekPlayer);
                            }
                        }
                        else
                        {
                            ChangeAIState(AIStates.seekAndShoot, AIStates.idle);
                        }
                    }
                    else
                    {
                        ChangeAIState(AIStates.seekAndShoot, AIStates.AvoidObstacle);
                    }
                    break;
                case AIStates.seekPlayer:
                    if (CanMoveForward())
                    {
                        if (vectorToTarget.magnitude <= seekDistance)
                        {
                            DoSeekPlayer();
                            if (IsStill())
                            {
                                ChangeAIState(AIStates.seekPlayer, AIStates.idle);
                            }
                            else if (angleToTarget <= shootFOV)
                            {
                                ChangeAIState(AIStates.seekPlayer, AIStates.seekAndShoot);
                            }
                        }
                        else
                        {
                            ChangeAIState(AIStates.seekPlayer, AIStates.idle);
                        }
                    }
                    else
                    {
                        ChangeAIState(AIStates.seekPlayer, AIStates.AvoidObstacle);
                    }
                    break;
                case AIStates.shoot:
                    data.mover.RotateTowards(vectorToTarget);
                    if (angleToTarget <= shootFOV && CanSeePlayer())
                    {
                        DoShoot();
                        if (vectorToTarget.magnitude <= seekDistance)
                        {
                            ChangeAIState(AIStates.shoot, AIStates.seekPlayer);
                        }
                    }
                    else
                    {
                        ChangeAIState(AIStates.shoot, AIStates.idle);
                    }
                    break;
                case AIStates.AvoidObstacle:
                    DoAvoidObstacle(prevState);
                    break;
            } 
        }
	}

    private void OnDestroy()
    {
        DoOnDestroy();
    }
}