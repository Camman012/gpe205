﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour {

    public enum AIStates { idle, patrol, shoot, seekPlayer, seekAndShoot, AvoidObstacle };
    public enum LoopTypes { stop, loop, pingPong, random };

    [HideInInspector] public TankData data;
    [HideInInspector] public float timer;
    [HideInInspector] public Ray leftAvoidSensor;
    [HideInInspector] public Ray rightAvoidSensor;
    [HideInInspector] public Ray rearAvoidSensor_Right;
    [HideInInspector] public Ray rearAvoidSensor_Left;
    [HideInInspector] public bool isNextToObstacle;
    [HideInInspector] public float lastAIStateChange;
    [HideInInspector] public Vector3 prevLocation;

    [HideInInspector] public List<GameObject> players;
    [HideInInspector] public List<Vector3> vectorsToPlayers;
    [HideInInspector] public Vector3 vectorToTarget;
    [HideInInspector] public GameObject target;
    [HideInInspector] public float angleToTarget;
    [HideInInspector] public Vector3 lastTargetLocation;
    [HideInInspector] public bool hasLostTarget;

    public AIStates aiState;
    public AIStates prevState;
    public float timeBetweenAIStateChange;

    [Header("Patrol Data")]
    public LoopTypes loopType;
    public GameObject idleFacingDirection;
    public List<GameObject> waypoints;
    public int currentWaypoint;
    public float closeEnough;
    public bool isPatrolingForward = true;
    [Header("Avoidance Data")]
    public float avoidDistance = 5.0f;
    public float forwardFacingSensorOffset = 1.0f;
    public float rearSensorOffset = 1.0f;
    [Header("Sense Data")]
    public float viewDistance = 15;
    public float FOV = 60;
    public float shootFOV = 10;
    
    public void DoStart () {
        Singleton.gm.GetComponent<GameData>().enemies.Add(gameObject);
        data = gameObject.GetComponent<TankData>();
        timer = 0;
        lastAIStateChange = 0;
        players = Singleton.gm.GetComponent<GameData>().players;
        target = players[0];
        currentWaypoint = 0;
        foreach (GameObject player in players)
        {
            vectorsToPlayers.Add(new Vector3());
        }
    }

    public void DoUpdate()
    {
        timer += Time.deltaTime;    //increase timer
        lastAIStateChange += Time.deltaTime; //increase last state change timer
        leftAvoidSensor = new Ray(data.mover.tf.position - data.mover.tf.right * forwardFacingSensorOffset, data.mover.tf.forward * avoidDistance); //left sensor for obstacal avoidance
        rightAvoidSensor = new Ray(data.mover.tf.position + data.mover.tf.right * forwardFacingSensorOffset, data.mover.tf.forward * avoidDistance);//right sensor for obstacal avoidance
        rearAvoidSensor_Right = new Ray(data.mover.tf.position - data.mover.tf.forward * rearSensorOffset, data.mover.tf.right * avoidDistance);    //rear right sensor for obstacal avoidance
        rearAvoidSensor_Left = new Ray(data.mover.tf.position - data.mover.tf.forward * rearSensorOffset, -data.mover.tf.right * avoidDistance);    //rear left sensor for obstacal avoidane

        vectorToTarget = target.transform.position - data.mover.tf.position;    //get vector to target
        angleToTarget = Vector3.Angle(data.mover.tf.forward, vectorToTarget);    //get angle to target
        for (int i = 0; i < players.Count; i++)
        {
            vectorsToPlayers[i] = (players[i].transform.position - data.mover.tf.position); //get vector to player i
            if (vectorsToPlayers[i].magnitude > vectorToTarget.magnitude)
            {
                target = players[i];    //set new target
            }
        }
    }

    public void DoOnDestroy()
    {
        Singleton.gm.GetComponent<GameData>().enemies.Remove(gameObject);
        if (Singleton.gm.GetComponent<GameData>().enemies.Count <= 0)
        {
            Singleton.gm.GetComponent<HighScore>().SaveHighScore();
            Singleton.gm.GetComponent<SceneChanger>().LoadScene(2);
        }
    }

    public void ChangeAIState(AIStates lastState, AIStates newState)
    {
        timeBetweenAIStateChange = 0;   //reset time since last state change
        aiState = newState;    //change to new state
        prevState = lastState; //store previous state
    }

    public bool CanMoveForward()
    {
        RaycastHit hitDataLeft;     //data of left sensor
        RaycastHit hitDataRight;    //data of right sensor
        if (Physics.Raycast(leftAvoidSensor, out hitDataLeft, avoidDistance))
        {
            InputController playerCheck;
            playerCheck = hitDataLeft.collider.gameObject.GetComponent<InputController>();
            if (playerCheck == null)
            {
                return false; //cannot move forward
            }
        }
        else if (Physics.Raycast(rightAvoidSensor, out hitDataRight, avoidDistance))
        {
            InputController playerCheck;
            playerCheck = hitDataRight.collider.gameObject.GetComponent<InputController>();
            if (playerCheck == null)
            {
                return false;   //cannot move forward
            }
        }
        return true;    //can move forward
    }

    public bool HasPassedObstacal(int avoidTurnDirection)
    {
        RaycastHit hitDataLeft;     //hit data for rear left ray
        RaycastHit hitDataRight;    //hit data for rear right ray
        if (avoidTurnDirection == 1)
        {
            if (Physics.Raycast(rearAvoidSensor_Left, out hitDataLeft))
            {
                isNextToObstacle = true;    //tank is next to obstacle
                if (!hitDataLeft.collider.gameObject.GetComponent<InputController>())
                {
                    return false;   //tank has not passed obstale
                }
            } 
        }
        else
        {
            if (Physics.Raycast(rearAvoidSensor_Right, out hitDataRight))
            {
                isNextToObstacle = true;    //tank is next to obstacle
                if (!hitDataRight.collider.gameObject.GetComponent<InputController>())
                {
                    return false;   //tank has not passed obstacle
                }
            } 
        }
        if (!isNextToObstacle)
        {
            return false;   //tank has not passed obstacle
        }
        isNextToObstacle = false;   //reset next to obstacle check
        return true;    //tank has passed obstacal
    }

    public bool CanSeePlayer()
    {
        if (angleToTarget <= FOV)
        {
            RaycastHit hitData; //raycast hit data
            if (Physics.Raycast(data.mover.tf.position, vectorToTarget, out hitData, viewDistance))
            {
                if (hitData.collider.gameObject.GetComponent<InputController>())
                {
                    return true;    //can see player 
                }
            }
        }
        return false;   //can not see player
    }

    public void DoIdle()
    {
        if (idleFacingDirection != null)
        {
            float distanceToIdlePoint = Vector3.Distance(data.mover.tf.position, waypoints[0].transform.position);  //get distance to idle position
            if (distanceToIdlePoint >= closeEnough)
            {
                Vector3 newDirection = waypoints[0].transform.position - data.mover.tf.position;    //get new direction
                data.mover.RotateTowards(newDirection); //turn to idle position
                data.mover.Move(data.mover.tf.forward); //move to idle position
            }
            else
            {
                Vector3 newDirection = idleFacingDirection.transform.position - data.mover.tf.position; //get direction to idle facing
                data.mover.RotateTowards(newDirection);         //turn towards idle facing direction
                data.mover.Move(Vector3.zero * data.forwardMoveSpeed); //stay in place 
            } 
        }
        else
        {
            data.mover.Move(Vector3.zero * data.forwardMoveSpeed); //stay in place
        }
    }

    public void DoPatrol()
    {
        if (waypoints.Count > 1 && waypoints[0] != null)
        {
            currentWaypoint = (int)Mathf.Clamp(currentWaypoint, 0, waypoints.Count);    //keep currentwaypoint in range
            Vector3 newDirection = waypoints[currentWaypoint].transform.position - data.mover.tf.position;  //get vector to next waypoint
            data.mover.RotateTowards(newDirection); //turn toward next waypoint
            data.mover.Move(data.mover.tf.forward * data.forwardMoveSpeed);    //move towards waypoint
            float distanceToWaypoint = Vector3.Distance(data.mover.tf.position, waypoints[currentWaypoint].transform.position); //distance between tank and waypoint
            if (distanceToWaypoint <= closeEnough)
            {
                switch (loopType)
                {
                    case LoopTypes.loop:
                        if (isPatrolingForward)
                        {
                            currentWaypoint++;  //set next waypoint
                            if (currentWaypoint >= waypoints.Count)
                            {
                                currentWaypoint = 0;    //go back to start waypoint
                            }
                        }
                        else
                        {
                            currentWaypoint--;  //set next waypoint
                            if (currentWaypoint == 0)
                            {
                                currentWaypoint = waypoints.Count - 1;  //go back to last waypoint
                            }
                        }
                        break;
                    case LoopTypes.pingPong:
                        if (isPatrolingForward)
                        {
                            currentWaypoint++;  //set next waypoint
                            if (currentWaypoint >= waypoints.Count)
                            {
                                isPatrolingForward = false; //now patrolling backwards
                            }
                        }
                        else
                        {
                            currentWaypoint--;  //set next waypoing
                            if (currentWaypoint == 0)
                            {
                                isPatrolingForward = true;  //now patrolling forwards
                            }
                        }
                        break;
                    case LoopTypes.random:
                        currentWaypoint = Random.Range(0, waypoints.Count - 1); //set next waypoint
                        break;
                    case LoopTypes.stop:
                        if (isPatrolingForward)
                        {
                            currentWaypoint++;  //set next waypoint
                            if (currentWaypoint >= waypoints.Count)
                            {
                                isPatrolingForward = false; //now patrolling backwards
                                ChangeAIState(AIStates.patrol, AIStates.idle);   //change to idle AI state
                            }
                        }
                        else
                        {
                            currentWaypoint--;  //set next waypoing
                            if (currentWaypoint == 0)
                            {
                                isPatrolingForward = true;  //now patrolling forwards
                                ChangeAIState(AIStates.patrol, AIStates.idle);   //change to idle AI state
                            }
                        }
                        break;
                }
            } 
        }
    }

    public void DoShoot()
    {
        if (timer >= data.timeBetweenShots)
        {
            timer = 0;
            data.shooter.FireBullet();
        }
    }

    public void DoSeekPlayer()
    {
        if (CanSeePlayer())
        {
            Vector3 newDirection = target.transform.position - data.mover.tf.position; //get vector to player
            data.mover.RotateTowards(newDirection); //turn toward player
            data.mover.Move(data.mover.tf.forward); //move forward
            hasLostTarget = false;
        }
        else
        {
            if (!hasLostTarget)
            {
                lastTargetLocation = target.transform.position;
                hasLostTarget = true;
            }
            Vector3 newDirection = lastTargetLocation - data.mover.tf.position; //get vector to last known player location
            data.mover.RotateTowards(newDirection); //turn towardplayer
            data.mover.Move(data.mover.tf.forward); //move forward
        }
    }

    public bool IsStill()
    {
        Vector3 currentLocation = data.mover.tf.position;
        Vector3 movement = currentLocation - prevLocation;
        prevLocation = currentLocation;
        if (movement.magnitude > 0)
        {
            return false;
        }
        return true;
    }

    public void DoAvoidObstacle(AIStates prevState)
    {

        int direction = Random.Range(0, 1); //choose a random direction to turn for avoidance
        if (!CanMoveForward())
        {
            RaycastHit leftHitData;
            RaycastHit rightHitData;
            if (Physics.Raycast(leftAvoidSensor, out leftHitData) && !Physics.Raycast(rightAvoidSensor, out rightHitData))
            {
                direction = -1;
                data.mover.Rotate(-1);   //turn right
            }
            else if (Physics.Raycast(rightAvoidSensor, out rightHitData) && !Physics.Raycast(leftAvoidSensor, out leftHitData))
            {
                direction = 1;
                data.mover.Rotate(1);  //turn left
            }
            else
            {
                if (direction == 0)
                {
                    data.mover.Rotate(-1);  //turn left
                }
                else
                {
                    data.mover.Rotate(1);   //turn right
                }  
            }
        }
        else
        {
            data.mover.Move(data.mover.tf.forward); //move forward
            if (HasPassedObstacal(direction))
            {
                ChangeAIState(AIStates.AvoidObstacle, prevState);   //go back to previous AI state
            }
        }
    }
}