﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController_Sniper : AIController {

    public float shootRange;

	// Use this for initialization
	void Start () {
        DoStart();  //perform start functions from base class
	}
	
	// Update is called once per frame
	void Update () {
        if (!Singleton.gm.GetComponent<Pause>().isPaused)
        {
            DoUpdate(); //perform update functions from base class

            switch (aiState)
            {
                case AIStates.idle:
                    DoIdle();   //preform idle actions
                    if (CanSeePlayer())
                    {
                        if (vectorToTarget.magnitude > shootRange)
                        {
                            ChangeAIState(AIStates.idle, AIStates.seekPlayer);  //seek out the player
                        }
                        else
                        {
                            ChangeAIState(AIStates.idle, AIStates.shoot);
                        }
                    }
                    break;
                case AIStates.patrol:
                    if (waypoints.Count > 1)
                    {
                        if (CanMoveForward())
                        {
                            DoPatrol();
                            if (CanSeePlayer())
                            {
                                if (vectorToTarget.magnitude > shootRange)
                                {
                                    ChangeAIState(AIStates.patrol, AIStates.seekPlayer);  //seek out the player
                                }
                                else
                                {
                                    ChangeAIState(AIStates.patrol, AIStates.shoot); //shoot player
                                }
                            }
                        }
                        else
                        {
                            ChangeAIState(AIStates.patrol, AIStates.AvoidObstacle);
                        }
                    }
                    else
                    {
                        ChangeAIState(AIStates.patrol, AIStates.idle);
                    }
                    break;
                case AIStates.seekPlayer:
                    if (CanMoveForward())
                    {
                        DoSeekPlayer();
                        if (IsStill())
                        {
                            ChangeAIState(AIStates.seekPlayer, AIStates.idle);
                        }
                        else if (vectorToTarget.magnitude <= shootRange)
                        {
                            ChangeAIState(AIStates.seekPlayer, AIStates.shoot);
                        }
                        else
                        {
                            ChangeAIState(AIStates.seekPlayer, AIStates.idle);
                        }
                    }
                    else
                    {
                        ChangeAIState(AIStates.seekPlayer, AIStates.AvoidObstacle);
                    }
                    break;
                case AIStates.shoot:
                    if (CanSeePlayer())
                    {
                        if (vectorToTarget.magnitude <= shootRange && angleToTarget <= shootFOV)
                        {
                            DoShoot();
                        }
                        else
                        {
                            ChangeAIState(AIStates.shoot, AIStates.seekPlayer);
                        }
                    }
                    else
                    {
                        ChangeAIState(AIStates.shoot, AIStates.idle);
                    }
                    break;
                case AIStates.AvoidObstacle:
                    DoAvoidObstacle(prevState);
                    break;
            } 
        }
    }

    private void OnDestroy()
    {
        DoOnDestroy();
    }
}