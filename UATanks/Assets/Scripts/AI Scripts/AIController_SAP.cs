﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController_SAP : AIController {

    private void Start()
    {
        DoStart();  //perform start functions from base class
    }

    // Update is called once per frame
    void Update ()
    {
        if (!Singleton.gm.GetComponent<Pause>().isPaused)
        {
            DoUpdate();    //perform update functions from base class

            switch (aiState)
            {
                case AIStates.idle:
                    DoIdle();   //preform idle actions
                    if (CanSeePlayer())
                    {
                        ChangeAIState(AIStates.idle, AIStates.seekPlayer);  //change to seek player state
                    }
                    else if (lastAIStateChange >= timeBetweenAIStateChange)
                    {
                        ChangeAIState(AIStates.idle, AIStates.patrol);
                    }
                    break;
                case AIStates.patrol:
                    if (waypoints.Count > 1)
                    {
                        if (CanSeePlayer())
                        {
                            ChangeAIState(AIStates.patrol, AIStates.seekPlayer);    //seek out the player
                        }
                        else if (CanMoveForward())
                        {
                            DoPatrol(); //patrol as normal
                        }
                        else
                        {
                            ChangeAIState(AIStates.patrol, AIStates.AvoidObstacle); //move to avoid obstacle
                        }
                    }
                    else
                    {
                        ChangeAIState(AIStates.patrol, AIStates.idle);
                    }
                    break;
                case AIStates.seekPlayer:
                    if (CanMoveForward())
                    {
                        DoSeekPlayer(); //seek out the player
                        if (IsStill())
                        {
                            ChangeAIState(AIStates.seekPlayer, AIStates.idle);  //return to previous state
                        }
                    }
                    else
                    {
                        ChangeAIState(AIStates.seekPlayer, AIStates.AvoidObstacle); //move to avoid obstacle
                    }
                    break;
                case AIStates.seekAndShoot:
                    if (CanSeePlayer())
                    {
                        if (angleToTarget > shootFOV)
                        {
                            ChangeAIState(AIStates.seekAndShoot, AIStates.seekPlayer);  //seek the player
                        }
                        DoSeekPlayer(); //seek player
                        DoShoot();      //shoot 
                    }
                    else
                    {
                        ChangeAIState(AIStates.seekAndShoot, AIStates.patrol);
                    }
                    break;
                case AIStates.AvoidObstacle:
                    DoAvoidObstacle(prevState); //move to avoid obstacle
                    break;
            } 
        }
    }

    private void OnDestroy()
    {
        DoOnDestroy();
    }
}