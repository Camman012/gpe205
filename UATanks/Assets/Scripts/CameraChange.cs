﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraChange : MonoBehaviour {

    public Camera cam;
    public float width;
    public float widthStart;

	// Use this for initialization
	void Start () {
		if (Singleton.gm.GetComponent<GameData>().twoPlayer)
        {
            cam.rect = new Rect(widthStart, 0, width, 1);
        }
	}
	
	public void SetCameraRect()
    {
        cam.rect = new Rect(0, 0, 1, 1);
    }
}
