﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowHighScore : MonoBehaviour {

    public Text highScoreText;
    public Text player1ScoreText;
    public Text player2ScoreText;

	// Use this for initialization
	void Start () {
        highScoreText.text = "HighScore: " + Singleton.gm.GetComponent<GameData>().highScore;
        player1ScoreText.text = "Player 1 Score:\n" + Singleton.gm.GetComponent<GameData>().player1Score;
        player2ScoreText.text = "Player 2 Score:\n" + Singleton.gm.GetComponent<GameData>().player2Score;
	}
}