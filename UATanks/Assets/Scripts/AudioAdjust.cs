﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioAdjust : MonoBehaviour {

    public enum Volume { master, music, sfx };

    [SerializeField] Volume adjustedVolume; // volume to be adjusted

    Slider slider;      //slider component
    float startSlider;  //value of current mixer volume

	// Use this for initialization
	void Start () {
        slider = gameObject.GetComponent<Slider>();     //get slider component
        switch(adjustedVolume)
        {
            case Volume.master:
                startSlider = Singleton.gm.GetComponent<GameData>().masterVolume;
                break;
            case Volume.music:
                startSlider = Singleton.gm.GetComponent<GameData>().musicVolume;
                break;
            case Volume.sfx:
                startSlider = Singleton.gm.GetComponent<GameData>().sfxVolume;
                break;
        }
        slider.value = startSlider;                     //set slider to current mixer value
    }
	
	// Update is called once per frame
	public void SetVolume () {
        switch (adjustedVolume)
        {
            case Volume.master:
                Singleton.gm.GetComponent<GameData>().masterVolume = slider.value;
                break;
            case Volume.music:
                Singleton.gm.GetComponent<GameData>().musicVolume = slider.value;
                break;
            case Volume.sfx:
                Singleton.gm.GetComponent<GameData>().sfxVolume = slider.value;
                break;
        }
    }
}
