﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnPause : MonoBehaviour {

    public void DoUnPause()
    {
        Singleton.gm.GetComponent<Pause>().DoPause();
    }
}
