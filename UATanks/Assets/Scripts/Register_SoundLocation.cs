﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Register_SoundLocation : MonoBehaviour {

    public bool isMusic;

	// Use this for initialization
	void Start () {
        if (isMusic) { Singleton.gm.GetComponent<GameData>().musicContainer = gameObject; }
        else { Singleton.gm.GetComponent<GameData>().sfxContainer = gameObject; }
	}
}