﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Powerup {
	public string displayName;
	public float lifespan = 1.5f;

	public virtual void ApplyPowerup(TankData data) {
		Debug.Log (displayName + " powerup applied!");
	}

	public virtual void RemovePowerup(TankData data){
		Debug.Log (displayName + " powerup removed.");		
	}
}

[System.Serializable]
public class HealthPowerup : Powerup {
	public int healthToAdd;

	public override void ApplyPowerup (TankData data){
		base.ApplyPowerup (data);   //run parent function
		data.health.CurrentHealth += healthToAdd;   //add health
	}

	public override void RemovePowerup (TankData data) {
		// Run the function in the parent class
		base.RemovePowerup (data);
	}
}

[System.Serializable]
public class DamagePowerup: Powerup {
	public int damageBoost;

	public override void ApplyPowerup (TankData data){
		base.ApplyPowerup (data);           //run parent function
		data.shellDamage += damageBoost;    //add damage boost
	}

	public override void RemovePowerup (TankData data) {
		base.RemovePowerup (data);          //run parent function
		data.shellDamage -= damageBoost;    //remove damage boost
	}
}

[System.Serializable]
public class HomingRocketPowerup: Powerup
{
    public int numberOfHomingShots;

    public override void ApplyPowerup(TankData data)
    {
        base.ApplyPowerup(data);    //run parent function
        data.numberOfHomingBullets += numberOfHomingShots;  //add homing bullets
    }
    public override void RemovePowerup(TankData data)
    {
        base.RemovePowerup(data);   //run parent function
    }
}