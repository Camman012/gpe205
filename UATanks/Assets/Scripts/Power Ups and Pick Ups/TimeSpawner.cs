﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeSpawner : MonoBehaviour {

	public GameObject spawnPrefab;
	public float respawnTime;
	public bool spawnOnLoad;

	GameObject spawnedObject;
	float countdown;
	Transform tf;

	// Use this for initialization
	void Start () {
		if (spawnOnLoad) {
			Spawn ();   //spawn powerup
		}
		countdown = respawnTime;
		// Load transform component
		tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
		if (spawnedObject == null) {
			countdown -= Time.deltaTime;    //count down
		}
		if (countdown <= 0) {
			Spawn ();   //spawn powerup
		}
	}

	void Spawn() {
		spawnedObject = Instantiate (spawnPrefab, tf.position, tf.rotation, tf.parent) as GameObject;  //create powerup
		countdown = respawnTime;    //reset timer
	}
}
