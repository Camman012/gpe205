﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupManager : MonoBehaviour {

	[HideInInspector] public TankData data;

	public List<Powerup> powerups;

	// Use this for initialization
	void Start () {
		data = GetComponent<TankData> ();	
		powerups = new List<Powerup> ();
	}


	// Update is called once per frame
	void Update () {
		HandlePowerups ();  //handle all current powerups
    }
    public void HandlePowerups()
    {
        List<Powerup> powerupsToRemove = new List<Powerup>();   //make a new list of powerups
        
        foreach (Powerup pu in powerups)
        {
            pu.lifespan -= Time.deltaTime;  // Subtract from powerups lifespan
            
            if (pu.lifespan <= 0)
            {
                pu.RemovePowerup(data);     //remove designated powerup
                powerupsToRemove.Add(pu);   //add powerup to remove list
            }
        }
        foreach (Powerup pu in powerupsToRemove)
        {
            powerups.Remove(pu);    //remove designated powerups from powerup list
        }
    }
}