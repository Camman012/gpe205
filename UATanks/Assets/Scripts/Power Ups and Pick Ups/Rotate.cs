﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour {

    Transform tf;
    public float rotateSpeed;

	// Use this for initialization
	void Start () {
        tf = transform;
	}
	
	// Update is called once per frame
	void Update () {
        tf.Rotate(0, rotateSpeed * Time.deltaTime, 0, Space.World);
	}
}
