﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup_HomingRockets : Pickup {

    public HomingRocketPowerup puData;
    public AudioClip pickupSound;

    public override void OnTriggerEnter(Collider c)
    {
        //get powerup manager-
        PowerupManager puManager;
        puManager = c.gameObject.GetComponent<PowerupManager>();
        //-
        if (puManager != null)
        {
            puManager.powerups.Add(puData);        //add to powerup list
            puData.ApplyPowerup(puManager.data);   //apply powerup
            AudioSource.PlayClipAtPoint(pickupSound, transform.position);
            Destroy(gameObject);
        }
    }
}
