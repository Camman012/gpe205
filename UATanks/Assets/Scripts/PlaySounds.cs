﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class PlaySounds : MonoBehaviour {

    public AudioClip buttonClick;
    public GameObject camera;

    public void PlaySound()
    {
        camera.GetComponent<AudioSource>().PlayOneShot(buttonClick, Singleton.gm.GetComponent<GameData>().sfxVolume * Singleton.gm.GetComponent<GameData>().masterVolume);
    }
}
